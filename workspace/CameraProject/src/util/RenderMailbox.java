package util;

import java.util.PriorityQueue;

/**
 * This is a mailbox! Ever used one? It works simila.r to the real deal!
 * 
 * @author the dolphin king
 * @version 2016-11-14
 */
public class RenderMailbox{


	private volatile PriorityQueue<RenderMessage> postBox;
	
	
	/**
	 * Constructor for mailbox
	 */
	public RenderMailbox(){
		postBox = new PriorityQueue<RenderMessage>();
	}
	
	/**
	 * posts a message to the mailbox
	 * @param message byte[] consisting of a message
	 */
	public synchronized void post(RenderMessage message){
		postBox.add(message);
		notifyAll();
	}
	
	/**
	 * Fetches a message from the mailbox
	 * IF THE MAILBOX IS EMPTY THE CALLING THREAD WILL BE BLOCKED UTIL SOMEONE POSTS
	 * there is no try-fetch because fuck you!
	 * @return
	 */
	public synchronized RenderMessage fetch(){
		if (postBox.isEmpty()){
			try {
				wait();
				return postBox.poll();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				System.out.println("Mailbox wait failed");
			}
		}
		
		return postBox.poll();
		
	}
    
    public synchronized RenderMessage fetch(int timeout){
		if (postBox.isEmpty()){
			try {
				wait(timeout);
				return postBox.poll();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				System.out.println("Mailbox wait failed");
			}
		}
		
		return postBox.poll();
		
	}
    
	
	
	public synchronized RenderMessage tryFetch(){
		if(!postBox.isEmpty()){
			return postBox.poll();
		}
		return null;
	}
	
	
	

/*test of order - WORKS
	public static void main (String[] args){
		RenderMailbox rm = new RenderMailbox();
		long time1 = System.currentTimeMillis();
		for(int i=0;i<100000;i++){
			System.out.println("derp");
		}
		long time2 = System.currentTimeMillis();
		rm.post(new RenderMessage("first image" , new byte[1], time1));
		rm.post(new RenderMessage("second image" , new byte[1], time2));
		System.out.println(rm.fetch().getName());
	}
	*/
	
	
	
}
