package util;

public class RenderMessage implements Comparable<RenderMessage> {

	private byte[] message;
	private String name;
	private long timeStamp;
	private static int timeDiff;
	
	public RenderMessage(String name,byte[] message, long timeStamp){
		this.name = name;
		this.message = message;
		this.timeStamp = timeStamp;
		
		
	}
	
	public String getName(){
		return name;
	}
	
	public byte[] getMessage(){
		return message;
	}

	public int delay(){
		return (int)(System.currentTimeMillis()-timeStamp);
	}
	
	public long timeStamp(){
		return timeStamp;
	}
	
	@Override
	public int compareTo(RenderMessage arg0) {
		
		return (int)(timeStamp-arg0.timeStamp);
	}
	
	public boolean equals(Object o){
		if(!(o instanceof RenderMessage))
			return false;
		RenderMessage other = (RenderMessage)o;
		return timeStamp==other.timeStamp;
	}
	
	
	
	
}
