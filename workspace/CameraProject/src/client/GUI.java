
package client;

import java.io.*;
import java.net.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

import javax.swing.*;

import server.CameraMainThread;

import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;



/**
 * Hurr durr I am a Hoers
 * @author alexander
 * @version 2016-11-14
 */
public class GUI extends JFrame {

	private CameraModes modes;
	private JPanel frames;//Jpanel med alla images
	private RenderThread render;


	private HashMap<String,ImagePanel> images;//lista av alla images
	private HashMap<String,CameraDecorator> cameras;
	private HashMap<String,JLabel> cameraLabels;
	private HashMap<String,JPanel> cameraPanels;
	
	public GUI() {
		super();
		images = new HashMap<String,ImagePanel>();
		cameras = new HashMap<String,CameraDecorator>();
		cameraLabels = new HashMap<String,JLabel>();
		cameraPanels = new HashMap<String,JPanel>();
		modes = new CameraModes();
		render = new RenderThread(this, modes);
		this.getContentPane().setLayout(new BorderLayout());
		this.getContentPane().add(createButtons(), BorderLayout.SOUTH);
		
		
		
		frames = new JPanel();
		this.getContentPane().add(frames, BorderLayout.NORTH);
		frames.setLayout(new GridLayout(0,1));
		//frames.setMinimumSize(new Dimension(600,400));
		this.setMinimumSize(new Dimension(655,600));
	    render.start();
        this.setVisible(true);
        pack();
      
  
	}
	
	
	
	
	
	
	private JPanel createButtons(){
		JPanel buttons = new JPanel();
		buttons.setLayout(new GridLayout(0,6));
		JButton movieButton = new JButton("Movie");
		movieButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e)
            {
                System.out.println("FORCING MOVIE");
                modes.setMode(modes.MOVIE);
                
            }
        });
		
		
		
		
		JButton idleButton = new JButton("IDLE");
		idleButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e)
            {
                System.out.println("FORCING IDLE");
                modes.setMode(modes.IDLE);
                
            }
        });
		
		JButton autoButton = new JButton("AUTO");
		autoButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e)
            {
                System.out.println("FORCING AUTO");
                modes.setMode(modes.IDLE);
                modes.setAuto(true);
                
            }
        });
		
		JButton syncButton = new JButton("Synchronous");
		syncButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e)
            {
                System.out.println("FORCING SYNCHRONOUS");
                modes.setSync(CameraModes.SYNCHRONIZED);
                
            }
        });
		
		JButton aSyncButton = new JButton("Asynchronous");
		aSyncButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e)
            {
                System.out.println("FORCING ASYNCHRONOUS");
                modes.setSync(CameraModes.ASYNCHRONIZED);
                
            }
        });
		
		JButton autoSyncButton = new JButton("Auto-Sync");
		autoSyncButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e)
            {
                System.out.println("AUTO-SYNC");
                modes.setSync(CameraModes.AUTO_SYNC);
                
            }
        });
		
		
		
		
		JButton connectButton = new JButton("CONNECT");
		
		connectButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e)
            {
            	String name;
            	while (true){
            		String ip = JOptionPane.showInputDialog("Server IP-address", "localhost");
            		//int port = Integer.parseInt(JOptionPane.showInputDialog("port", "6080"));
            		int localport = 6080+Integer.parseInt(JOptionPane.showInputDialog("which camera would you like to connect to?", "1"));
            		name = JOptionPane.showInputDialog("cameraName", "");
            		while(name == null || name.isEmpty() || cameras.containsKey(name)){
            			name = JOptionPane.showInputDialog("You may not enter an empty string, or a cameraname that is already in use\nPlease enter a new cameraname", "");
            		}
            		//System.out.println("GUI attempting to connect to: " + ip + "/"+ port);
            		try {
						CameraDecorator newCam = new CameraDecorator(name,ip,localport,modes,render);
						cameras.put(name,newCam);
						frames.add(cameraPanel(name));
						break;
					} catch (IOException e1) {
						JOptionPane.showMessageDialog(null,"Failed to connect to camera: " + name);
					}
            	}
            	
            
        		if(images.size() > 1)
        		{
        			System.out.println("GUI ADAPTING");
        			frames.setLayout(new GridLayout(0,2));
        			pack();
        		}else{
        			//pack();
        		}
            }
        	
        });
		
		
		
		JButton discButton = new JButton("DISCONNECT");
		discButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e)
            {
                System.out.println("FORCING DISC");
                
                if(cameras.size()>0){
                	String[] c = new String[cameras.size()];
                	cameras.keySet().toArray(c);
                	
                	Object ans = JOptionPane.showInputDialog(null, "Choose camera to disconnect","The Choice of a Lifetime", JOptionPane.QUESTION_MESSAGE, null,c,c[0]);
                	if (ans == null) return;
                	String name = (String)ans;
                	if (name.isEmpty()) return; //derp hanterar cancel
                	CameraDecorator cam = cameras.get(name);
                	System.out.println("You wanted to disconnect " + name);
                	cameras.remove(name).disconnect();
                	//frames.remove(images.remove(name));//tr
                	frames.remove(cameraPanels.remove(name));//this instead of commented
                	cameraLabels.remove(name);
                	images.remove(name);
                	//frames.remove(cameraLabels.remove(name));
                	if(images.size() == 1)
                	{
                		frames.setLayout(new GridLayout(0,1));
                		setSize(new Dimension(655,600));
                		pack();
                		
                	}else if(images.size() == 0){
                		//make the last image dissapear aswell
                	}
                	pack();
                		
                		
                	}else{
                		System.out.println("ERROR, COULD NOT FIND CAMERA IN GUI.CAMERAS");
                	}
                	
                
                	
            }});
		
		
		
		
		
		JPanel returnPanel = new JPanel();
		returnPanel.setLayout(new BorderLayout());
		JPanel bottomPanel = new JPanel();
		bottomPanel.setLayout(new GridLayout(0,2));
		bottomPanel.add(connectButton);
		bottomPanel.add(discButton);
		
		
		
		returnPanel.add(bottomPanel,BorderLayout.SOUTH);
		
		
		buttons.add(movieButton);
		buttons.add(idleButton);
		buttons.add(autoButton);
		buttons.add(syncButton);
		buttons.add(aSyncButton);
		buttons.add(autoSyncButton);
		returnPanel.add(buttons,BorderLayout.CENTER);
		return returnPanel;
	}
	
	
	
	
	public void updateImage(String name, byte[] data, int delay){
		
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				images.get(name).refresh(data);
				cameraLabels.get(name).setText("delay: " + delay + "ms");
				
				
			}
		});
		
		
		  
	}
	
	
	
	private JPanel cameraPanel(String name){
		JPanel temp = new JPanel();
		temp.setLayout(new BorderLayout());
		
		JLabel text = new JLabel("delay: 0ms");
		text.setVisible(true);
		ImagePanel im = new ImagePanel();
		cameraLabels.put(name,text);
		images.put(name,im);
		cameraPanels.put(name, temp);
		temp.add(im, BorderLayout.CENTER);
		temp.add(text, BorderLayout.SOUTH);
		im.setVisible(true);
		//temp.setMinimumSize(new Dimension(640,500));
		//temp.setVisible(true);
		return temp;

	}
}
	

class ImagePanel extends JPanel {
	ImageIcon icon;

	public ImagePanel() {
		super();
		icon = new ImageIcon();
		JLabel label = new JLabel(icon);
		add(label, BorderLayout.CENTER);
		this.setSize(new Dimension(640, 480));
		
	}

	public void refresh(byte[] data) {
		Image theImage = getToolkit().createImage(data);
		getToolkit().prepareImage(theImage,-1,-1,null);	    
		icon.setImage(theImage);
		icon.paintIcon(this, this.getGraphics(), 5, 5);
	}
}





