package client;

import java.io.IOException;
import java.io.OutputStream;


/**
 * CameraControl is the outPut thread of the Client, this thread writes control packages to the camera
 * The current control packages are, "Change mode to movie", "Change mode to idle", and "Terminate connection"
 * This is yet to be implemented
 * @author alexander
 * @version 2016-11-14
 */

public class CameraControl extends Thread {
	private int mode;
	private CameraModes modes;
	private OutputStream os;
	
	/**
	 * Constructor for CameraControl
	 * @param o the Outputstream to write to the camera
	 * @param modes Monitor with shared data for the client
	 */
	public CameraControl(OutputStream o, CameraModes modes){
		this.modes = modes;
		os = o;
		mode = modes.getMode();
		try {
			writeMode();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("cameraControl couldn't write mode on startup");
		}
		this.start();
	}
	
	/**
	 * Herpderp TODO, basically check if mode has changed in the monitor, if it has you should tell the camera to change.
	 * ALSO FIX TERMINATION
	 */
	public void run(){
		while (true){
			int newMode = modes.sleepThenGetMode();//we have to wake the thread if it is to disconnect(it will be sleeping when we disc)
			if (newMode!=mode){
				System.out.println("MODE CHANGED FROM: " + mode + " TO " + newMode);
				mode = newMode;
				try {
					writeMode();
				} catch (IOException e) {//if output is closed
					break;
				}
			}else {
				//fix so that the cameracontrol closes 
			}
		}
		System.out.println("CAMERACONTROL CLOSED");
	}
	
	
	private void writeMode() throws IOException{
		switch(mode){
			case CameraModes.MOVIE:
				System.out.println("Writing F to clients output");
				os.write("F".getBytes());//writes one Byte
				break;
			case CameraModes.IDLE:
				System.out.println("Writing I to clients output");
				os.write("I".getBytes());
				break;
			default:
				throw new IllegalArgumentException("mode has the wrong value in CaemeraControl");
		}
	}
	
	
	
	
	
}
