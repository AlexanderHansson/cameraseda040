package client;

import java.io.IOException;
import java.net.Socket;


/**
 * This is a decorator-class for a camera.
 * The main purpose is to create all objects "client sided" needed for a camera
 * EG: 
 * 
 * 1 Socket for connecting to the camera
 * 1 CameraControl
 * 1 FrameGrabber
 * 
 * It also contains a "name"-String of the camera, in order for the client to name and indentify it.
 * 
 * 
 * The reason this class exists, is to make it easier to create serveral cameras.
 * 
 * @author alexander
 * @version 2016-11-14
 */
public class CameraDecorator {
	private String cameraName;
	private Socket s;
	private CameraControl c;
	private FrameGrabber f;
	private String ip;
	private CameraModes modes;
	/**
	 * Constructor for a CameraDecorator
	 * @param name The cameras name
	 * @param ip The cameras IP-address
	 * @param port The cameras network-port
	 * @param m Monitor with shared data in the client
	 * @param render The render-thread
	 * @throws IOException If it's not possible to connect to the given ip/port
	 */
	public CameraDecorator(String name, String ip, int port, CameraModes m, RenderThread render) throws IOException {		
		cameraName = name;
		modes = m;
		s = new Socket("localhost",port);
		c = new CameraControl(s.getOutputStream(),m);//tr�d f�r att prata med kameran
		
		f = new FrameGrabber(cameraName,s.getInputStream(),m,render); //tr�d f�r att h�mta bilder
		this.ip=ip;
	}
	
	
	/**
	 * 
	 * @return name of camera
	 */
	public String getName(){
		return cameraName;
	}
	
	public String getIp(){
		return ip;
	}
	
	public String toString(){
		return cameraName;
	}
	
	
	/**
	 * IMPLEMENT THIS
	 */
	public void disconnect(){
		try{
		s.shutdownInput();
		s.shutdownOutput();
		modes.wakeThreads();
		System.out.println("You sucesfully disconnected camera: " + cameraName);
		}catch(IOException e){
			System.out.println("Could not turn off camera " + cameraName);
		}
		
	}
	
}
