package client;

import java.io.IOException;
import java.io.InputStream;
import util.RenderMessage;

/**
 * FrameGrabbers main purpose is to listen to an outputstream for cameraFrames
 * If a frame has been sent, it will be passed to renderThread.
 * It will also detect "movement" packages from the camera, and alert CameraModes.movementDetected();
 * @author alexander
 * @version 2016-11-13
 */
public class FrameGrabber extends Thread {

	private InputStream is;
	private CameraModes modes;
	private RenderThread render;
	private String name;
	private boolean eos;
	private boolean firstFrame;
	private long delay;

	
	/**
	 * Constructor for FrameGrabber
	 * @param name the Name of the camera
	 * @param i The inputStream of containing messages from the camera
	 * @param m The monitor with shared client-data
	 * @param render The render-thread that renders images
	 */
	public FrameGrabber(String name, InputStream i, CameraModes m, RenderThread render) {
		delay=0;
		firstFrame=true;
		this.name = name;
		is = i;
		modes = m;
		this.render = render;
		eos = false;
		this.start();
	}

	/**
	 * Basically reads the inputStream and deciphers the first flag
	 * If picture: (flag B)
	 * 		call getPicture() that posts it to the render-thread
	 * If motionDetected: (flag M)
	 * 		call CameraModes.motionDetected()
	 */
	public void run() {
		while (!eos) {
			char ans;

			try {
				// read flag from cameras output
				ans = (char) is.read();
				System.out.println("\nFrameGrabber recieved " + ans);

				// switch flag to figure out what kind of package it is
				switch (ans) {
				case 'B':// picture frame
					getPicture();
					break;
				case 'M':// motion detected
					System.out.println("Motion detected in client");
					modes.motionDetected(name);
					break;
				case (char)(-1):
						System.out.println("reached end of stream");
						eos = true;
						break;
				default:
					System.out.println("Warning, false package flag in frameGrabber");
				}

			} catch (IOException e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
				System.out.println("could not read inputstream");
			}

		}
		System.out.println("FRAMEGRABBER CLOSED");

		// While true grab frames from inputstream and pass it to RenderThreads
		// mailbox

		// if frame isnt a picture, it is motion detected! then alert
		// CameraModes.motionDetected
	}

	
	/**
	 * This method is used to read a picture and post it to the render-thread
	 */
	private void getPicture() {

		// 4bytes length of picture, 8bytes timestamp + picture
		System.out.println("Getting picture");
		byte[] len = new byte[4];
		int read = 0;
		while (read < 4) {
			try {
				read += is.read(len, 0 + read, 4);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
			}

		}

		int length = (len[0] << 24) & 0xff000000 | (len[1] << 16) & 0x00ff0000 | (len[2] << 8) & 0x0000ff00
				| (len[3] << 0) & 0x000000ff;

		System.out.println("received image length: " + length);

		// reads timestamp
		byte[] timeStamp = new byte[8];
		read = 0;
		while (read < 8) {
			try {
				read += is.read(timeStamp, 0 + read, 4);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
			}

		}
		long ts = toTime(timeStamp);//timestamp in normal format
		
		
		
		//read image
		byte[] image = new byte[length];
		int i = 0;
		while (i < length) {
			try {
				length -= is.read(image, 0 + i, length);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
			}

		}

		
		System.out.println("image recieved");
		render.mailbox.post(new RenderMessage(name,image,ts));
		System.out.println("Render has got mail");

	}
	
	
	
	private long toTime(byte[] timeStamp){
		long time = 0;
		for (int i=0; i<timeStamp.length; i++){
			time = (time << 8) | (timeStamp[i] & 0xFF);
		}
		if (firstFrame){
			firstFrame=false;
			delay=(System.currentTimeMillis()-time);
			
		}else{
			long imagedelay=(System.currentTimeMillis()-time);
			delay = delay<imagedelay?delay:imagedelay;
		}
		
		System.out.println("DELAY IN FRAMEGRABBER " + delay);
		return time+delay;
	}
	

}
