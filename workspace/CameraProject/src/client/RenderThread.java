package client;

import java.lang.reflect.InvocationTargetException;
import java.util.LinkedList;

import javax.swing.SwingUtilities;

import util.*;

/**
 * Obey your master, master!
 * 
 * @author the master
 * @version 2016-11-14
 */
public class RenderThread extends Thread {

	public volatile RenderMailbox mailbox;// VOLATILE VIKTIGT!!!
	private GUI gui;
	private static final double threshold = 0.2;
	private static final int SYNCHRONOUS = 0;
	private static final int ASYNCHRONOUS = 1;
	private CameraModes monitor;
	int mode;

	// TODO hurr-durr
	// how do we solve what camera posted?
	// create a "message" class with image and camera name as message?
	// also handle synchronized/async!!!

	/**
	 * Constructor for RenderThread
	 * 
	 * @param gui
	 *            the gui
	 */
	public RenderThread(GUI gui, CameraModes mon) {
		mode = SYNCHRONOUS;
		monitor=mon;
		this.mailbox = new RenderMailbox();
		this.gui = gui;
		//this.start();
		this.setPriority(this.MAX_PRIORITY);

	}

	/**
	 * Not sure if this is the right way to access GUI. maybe pass a runnable?
	 */
	public void run() {
		int delay = 0;
		while (true) {
			RenderMessage message = mailbox.fetch();
			System.out.println("Render recieved an image from mailbox");
			int imageDelay = message.delay();
			System.out.println("delay: " + delay + "\nImageDelay: " + imageDelay);
			System.out.println("mode " + (mode==SYNCHRONOUS?"SYNC":"ASYNC"));
			
			
			
			if (mode == SYNCHRONOUS) {

				if (delay > imageDelay) {
					try {
						sleep(delay - imageDelay);
						gui.updateImage(message.getName(), message.getMessage(), delay); 
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						// e.printStackTrace();
					}

				} else {
					delay = imageDelay;
					gui.updateImage(message.getName(), message.getMessage(), delay);
				}
				if(monitor.getSync()==monitor.AUTO_SYNC){
					if (delay>threshold * 1000){
						System.out.println("DELAY: " + delay + " - SWITCHING TO ASYNCHRONOUS");
						mode = ASYNCHRONOUS;
					}
				}else{
					mode = monitor.getSync()==monitor.SYNCHRONIZED?SYNCHRONOUS:ASYNCHRONOUS;
				}
				
			}else{//async code here
				delay = imageDelay;
				if(monitor.getSync()==monitor.AUTO_SYNC){
					
					if (delay<((threshold * 1000)/2)){
						mode = SYNCHRONOUS;
						System.out.println("DELAY: " + delay + " - SWITCHING TO SYNCHRONOUS");
						
					}
				}else{
					mode = monitor.getSync()==monitor.SYNCHRONIZED?SYNCHRONOUS:ASYNCHRONOUS;
				}
				gui.updateImage(message.getName(), message.getMessage(), delay); 
			}

			
			

			System.out.println("Render finished posting");

		}

	}

	
	
}
