package client;



import javax.swing.SwingUtilities;

import server.CameraMainThread;



/**
 * This is a main class used for debug, it will be removed in the final implementation,
 * The REAL main is the GUI, which will (in the end) create all cameras and threads
 * @author alexander
 *
 */
public class Main {

	/**
	 * No point commenting since this thread will change alot during debug
	 * @param args
	 */
	public static void main(String[] args){


	
		SwingUtilities.invokeLater(new Runnable(){
			public void run(){
				new GUI();
			}
		});
		

		
		
	}
	
}
