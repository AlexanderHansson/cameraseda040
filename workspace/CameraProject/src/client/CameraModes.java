package client;



/**
 * This is the clients monitor class, it's a passive-object with shared information.
 * All the methods are Synchronized in order to handle multiple-access from different client-threads.
 * The main purpose of the class is to contain and manage the mode (movie/idle/auto) of the camera.
 * 
 * @author alexander
 * @version 2016-11-14
 */
public class CameraModes {

private volatile int mode;
public static final int MOVIE = 0;
public static final int IDLE = 1;
public static final int SYNCHRONIZED = 1;
public static final int ASYNCHRONIZED = 2;
public static final int AUTO_SYNC = 0;

private volatile int sync;
private volatile boolean auto;
private volatile String camera; //the camera that started movie mode;

	/**
	 * Constructor for CameraModes()
	 */
	public CameraModes(){
		sync = AUTO_SYNC;
		mode = IDLE;
		auto = true;
	}

	/**
	 * returns mode(movie or idle)
	 * @return an integer, 0 = movie, 1 = idle
	 */
	public synchronized int getMode(){
	
		return mode;
	}
	
	public synchronized void wakeThreads(){
		notifyAll();
	}

	public synchronized int sleepThenGetMode(){
		try {
			wait();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		}
		return mode;
	}
	
	
	
	/**
	 * Sets mode to movie if auto is true
	 * Saves the cameraname if moviemode is set
	 * @param camera The cameras name (In order to see who changed mode)
	 */
	public synchronized void motionDetected(String camera){
		if (camera == null)
			throw new IllegalArgumentException("You have to indentify the alerting camera");
		if (auto){
				mode = MOVIE;
				notifyAll();
		}	
	}
	
	
	//fix so you can only set one of the constants
	/**
	 * Sets mode no matter if auto is true or false. This is used by the buttons
	 * @param newMode The mode to set. 
	 */
	public synchronized void setMode(int newMode)
	{
		auto = false;
		mode = newMode;
		notifyAll();
	}
	
	/**
	 * Sets auto to param
	 * @param auto the value of the new auto
	 */
	public synchronized void setAuto(boolean auto){
		this.auto = auto;
	}

	
	/**
	 * return true if auto is on
	 * @return
	 */
	public synchronized boolean getAuto(){
		return auto;
	}
	
	
	public synchronized void setSync(int sync){
		if(!(sync==AUTO_SYNC || sync==SYNCHRONIZED || sync==ASYNCHRONIZED)){
			throw new IllegalArgumentException("must be one of the sync values in CameraModes");
			
		}else{
			this.sync = sync;
		}
	}
	
	public synchronized int getSync(){
		return sync;
	}
	
	

}
