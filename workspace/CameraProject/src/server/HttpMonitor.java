package server;

public class HttpMonitor {

	private volatile byte[] image;
	
	
	public HttpMonitor(){
		
	}
	
	public synchronized void post(byte[] image){
		this.image=image;
		notifyAll();
	}
	
	public synchronized byte[] getImage(){
		if (image==null){
			try {
				wait();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return image;
		
	}
	
	
}
