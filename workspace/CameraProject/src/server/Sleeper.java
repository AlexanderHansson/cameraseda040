package server;

public class Sleeper {

	
	/**
	 * Sleeper is a monitor used to put threads to sleep when they are not needed.
	 */
	public synchronized void sleep(){
		try {
			wait();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			System.out.println("Sleeper couldnt sleep");
			//e.printStackTrace();
		}
	}
	
	public synchronized void wake(){
		notifyAll();
	}
}
