package server;

import java.io.IOException;
import java.io.OutputStream;
import se.lth.cs.eda040.proxycamera.*;


public class CameraOut extends Thread {

	public volatile CameraMailbox mailbox; //public so other threads can post to it
	private HttpMonitor httpMonitor;
	private OutputStream os;
	private int sleep;
	private AxisM3006V myCamera;
	private Sleeper mainSleeper;
	private static final int LONG_SLEEP = 5000;
	private static final int SHORT_SLEEP = 84;//12fps

	/**
	 * Constructor for CameraOut
	 * @param o The outputstream for the camera
	 * @param port The network-port
	 */
	public CameraOut(OutputStream o, int port, String camIp, int camport,Sleeper sl, HttpMonitor mon) {
		httpMonitor= mon;
		mailbox = new CameraMailbox();
		mainSleeper = sl;
		os = o;
		sleep = LONG_SLEEP;// change to cameras prefered values
		myCamera = new AxisM3006V();
		myCamera.init();
		myCamera.setProxy(camIp, camport);
		myCamera.connect();
		this.start();
	}

	
	
	
	public void run() {
		// check mailbox, fetch()
		// if there is a mode change, change the sleep delay
		while (!isInterrupted()) {
			System.out.println("CameraOut is alive");
			try {
				postPicture();
				System.out.println("CameraOut posted picture");
				changeMode();
				System.out.println("Cameraout Changed mode");
			} catch (InterruptedException | IOException e) {
					if(e instanceof IOException){
					System.out.println("CameraOut got interrupted");
					break;
				}else{
					System.out.println("cameraOut.changeMode() FAILED");
				}
			}

		}
		System.out.println("CAMERAOUT SHUT DOWN");
		myCamera.close();
		myCamera.destroy();
		mainSleeper.wake();
	}

	/**
	 * Checks the mailbox and changes mode according to the mail.
	 */
	private void changeMode() throws InterruptedException{
		byte message = mailbox.fetch(sleep);
			char flag = (char)message;
			System.out.println("Camera detected mode " + flag);
			switch(flag){
				case 'F':
					sleep=SHORT_SLEEP;
					break;
				case 'I':
					sleep=LONG_SLEEP;
					break;
				default:
					break;
			}
			
		
	}
	
	
	/**
	 * This method posts a motionDetected package to the outputStream.
	 * A motionDetected-package consists of the letter "M" represented by one byte.
	 */
	public void postMotion() {
		System.out.println("Motion detected in server");
		try {
			os.write("M".getBytes());
		} catch (IOException e) {
			System.out.println("postMotion in cameraOut failed");
			// TODO Auto-generated catch block
			// e.printStackTrace();
		}
	}

	/**
	 * This method posts a Picture to the output stream.
	 * A picture package is represented by 1+4+8+x bytes
	 * *
	 * Byte 1: The character 'B' - to flag that it's a picture package
	 * Byte 2-5: The length of the jpg image.
	 * Byte 6-13: The timestamp of the picture (always 8byte)
	 * Byte 14-x: The image
	 */
	public void postPicture() throws IOException {
		
			// writes 1byte flag, 4bytes length of picture, 8bytes timestamp + picture

			byte[] jpeg = new byte[AxisM3006V.IMAGE_BUFFER_SIZE];
			System.out.println(AxisM3006V.IMAGE_BUFFER_SIZE);
			int len = myCamera.getJPEG(jpeg, 0);
			
			//posting to HTTP monitor
			byte[] httpImage = new byte[len];
			for(int i=0;i<len;i++){
				httpImage[i] = jpeg[i];
			}
			httpMonitor.post(httpImage);
			
			
			
			// if motion is detected
			if (myCamera.motionDetected()) postMotion();
			
			
			os.write("B".getBytes()); // writes 'B' to represent picture package
			System.out.println("Image length: " + len);

			byte[] length = { (byte) (len >>> 24), (byte) (len >>> 16), (byte) (len >>> 8), (byte) len }; //converts the length of the image to bytes
			os.write(length, 0, 4); //writes the images length

			//get timestamp
			byte[] timeStamp = new byte[AxisM3006V.TIME_ARRAY_SIZE];
			long currTime = System.currentTimeMillis();
			myCamera.getTime(timeStamp, 0);
			
		
			//debug
		
			
			System.out.println("\n");
			//debug
			
			
			os.write(timeStamp);//writes timestamp
			os.write(jpeg, 0, len);//writes image
		
	}

}
