package server;

import java.io.IOException;
import java.io.InputStream;

/**
 * CameraIn is the reading-thread of the camera It listens to the cameras
 * inputStream and waits for a command-message from the client If a
 * "change-mode" package has been sent, the CameraIn will post to the CameraOuts
 * mailbox in order to alert it.
 * 
 * @author alexander
 * @version 2016-11-14
 */
public class CameraIn extends Thread {

	private InputStream is;
	private CameraOut out;

	/**
	 * Constructor for cameraIn
	 * 
	 * @param i
	 *            The cameras InputStream
	 * @param out
	 *            The cameraOut thread. used to access mailbox
	 */
	public CameraIn(InputStream i, CameraOut out) {
		is = i;
		this.out = out;
		this.start();
	}

	/**
	 * TODO- read inputstream, Decipher the bytes-read act based on the package
	 * 
	 */
	public void run() {
		while (true) {
			try {
				System.out.println("camera reading input");
				char flag = (char) is.read();
				System.out.println("camera finished read input");
				
				if (flag==(char)(-1)){
					System.out.println("CameraIn closing");
					break;
				}
				/*
				switch (flag) {
				case 'F':
					System.out.println("Camera detected movie mode");
					break;
				case 'I':
					System.out.println("Camera detected Idle mode");
				}
				*/
				out.mailbox.post((byte)flag);//posts mode to camera
				
			} catch (IOException e) {
				System.out.println("CameraIn could not read inputStream");
			}

		}
		System.out.println("CAMERAIN CLOSED");
		try {
			is.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// read is
		// tolka kommandon
		// ifall vi ska andra mode sa maste dessa skicka till CameraOuts mailbox
		// mha CameraOut.post()
	}

}
