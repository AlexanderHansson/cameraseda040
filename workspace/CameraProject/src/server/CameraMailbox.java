package server;

/**
 * This is a mailbox! Ever used one? It works simila.r to the real deal!
 * 
 * @author the dolphin king
 * @version 2016-11-14
 */
public class CameraMailbox{
	
	private volatile byte[] postBox;
	private volatile int end = 0;
	
	/**
	 * Constructor for mailbox
	 */
	public CameraMailbox(){
		postBox = new byte[10];
	}
	
	/**
	 * posts a message to the mailbox
	 * @param message byte[] consisting of a message
	 */
	public synchronized void post(byte message){
		if(end>=postBox.length)
		{
			byte[] temp = postBox;
			postBox = new byte[postBox.length*2];
			end =0;
			for (byte b :temp){
				postBox[end++]=b;
			}
		}
		postBox[end]=message;
		end++;
		notifyAll();
	}
	
	/**
	 * Fetches a message from the mailbox
	 * IF THE MAILBOX IS EMPTY THE CALLING THREAD WILL BE BLOCKED UTIL SOMEONE POSTS
	 * there is no try-fetch because fuck you!
	 * @return
	 */
	public synchronized byte fetch(){
		if (end==0){
			try {
				wait();
				byte ret = postBox[0];
				for(int i=0; i<end; i++){//check if this is ok
					postBox[i] = postBox[i+1];
				}
				end--;
				return ret;
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				System.out.println("Mailbox wait failed");
			}
		}
		
		byte ret = postBox[0];
		for(int i=0; i<end; i++){//check if this is ok
			postBox[i] = postBox[i+1];
		}
		end--;
		
		return ret;
		
	}
    
    public synchronized byte fetch(int timeout){
    	if (end==0){
			try {
				wait(timeout);
				if (end==0){
					return '?';
				}
				byte ret = postBox[0];
				for(int i=0; i<end; i++){//check if this is ok
					postBox[i] = postBox[i+1];
				}
				end--;
				return ret;
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				System.out.println("Mailbox wait failed");
			}
		}
		
		byte ret = postBox[0];
		for(int i=0; i<end; i++){//check if this is ok
			postBox[i] = postBox[i+1];
		}
		end--;
		
		return ret;
	}
    
	
	

	
}