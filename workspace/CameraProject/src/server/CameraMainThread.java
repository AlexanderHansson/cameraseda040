package server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;



public class CameraMainThread extends Thread {

	private int clientPort;//byta till kamerans port senare
	private String camIp;
	private int camport;
	public Sleeper mainSleeper;
	
	
	
	public static void main(String[]args){
		new CameraMainThread("argus-1.student.lth.se",6081,6081);
		new CameraMainThread("argus-2.student.lth.se",6082,6082);
		new CameraMainThread("argus-3.student.lth.se",6083,6083);
		
	}
	
	/**
	 * Constructor for MainThread
	 */
	public CameraMainThread(String ip, int port, int clientPort){
		camIp = ip;
		camport = port;
		this.clientPort = clientPort;
		mainSleeper = new Sleeper();
		System.out.println("Server started");
		this.start();
	}
	
	/**
	 * Pretty much finished, creates the threads for the camera
	 */
	public void run(){	
		ServerSocket ss;
		while (true)
		{
			try {
				ss = new ServerSocket(clientPort);
				System.out.println("serversocket created");
				Socket s = ss.accept();
				System.out.println("Server accepted request");
				System.out.println("Server started:\nPort: " + clientPort + "\nIp: " + s.getInetAddress() );
				HttpMonitor mon = new HttpMonitor();
				new HttpServer(mon, clientPort+10);
				CameraOut out = new CameraOut(s.getOutputStream(), clientPort, camIp, camport,mainSleeper,mon);
				new CameraIn(s.getInputStream(),out);
				System.out.println("CameraIn and Out created");
				mainSleeper.sleep();
				ss.close();
			
			
			} catch (IOException e) {
				System.out.println("Could not establish network connection");
				//e.printStackTrace();
			}
		
		}
		
	}
		

	
	
	
}
