package server;

import java.net.*; // Provides ServerSocket, Socket
import java.io.*; // Provides InputStream, OutputStream

import se.lth.cs.eda040.fakecamera.*; // Provides AxisM3006V

/**
 * Itsy bitsy teeny weeny web server. Always returns an image, regardless of the
 * requested file name.
 */
public class HttpServer extends Thread {

	HttpMonitor monitor;
	int myPort;

	public HttpServer(HttpMonitor mon, int port) {
		monitor = mon;
		myPort = port;
		this.start();
	}

	public void run() {
		

		ServerSocket serverSocket;

		System.out.println("HTTP server operating at port " + myPort + ".");
		
		while (true) {
			try {
				serverSocket = new ServerSocket(myPort);
				while (true) {
					byte[] jpeg = monitor.getImage();
					
					// The 'accept' method waits for a client to connect, then
					// returns a socket connected to that client.
					Socket clientSocket = serverSocket.accept();
					
					// The socket is bi-directional. It has an input stream to
					// read
					// from and an output stream to write to. The InputStream
					// can
					// be read from using read(...) and the OutputStream can be
					// written to using write(...). However, we use our own
					// getLine/putLine methods below.
					InputStream is = clientSocket.getInputStream();
					OutputStream os = clientSocket.getOutputStream();

					// Read the request
					String request = getLine(is);

					// The request is followed by some additional header lines,
					// followed by a blank line. Those header lines are ignored.
					String header;
					boolean cont;
					do {
						header = getLine(is);
						cont = !(header.equals(""));
					} while (cont);

					System.out.println("HTTP request '" + request + "' received.");

					// Interpret the request. Complain about everything but GET.
					// Ignore the file name.
					if (request.substring(0, 4).equals("GET ")) {
						// Got a GET request. Respond with a JPEG image from the
						// camera. Tell the client not to cache the image
						putLine(os, "HTTP/1.0 200 OK");
						putLine(os, "Content-Type: image/jpeg");
						putLine(os, "Pragma: no-cache");
						putLine(os, "Cache-Control: no-cache");
						putLine(os, ""); // Means 'end of header'

						os.write(jpeg);

					} else {
						// Got some other request. Respond with an error
						// message.
						putLine(os, "HTTP/1.0 501 Method not implemented");
						putLine(os, "Content-Type: text/plain");
						putLine(os, "");
						putLine(os, "No can do. Request '" + request + "' not understood.");

						System.out.println("Unsupported HTTP request!");
					}

					os.flush(); // Flush any remaining content
					clientSocket.close(); // Disconnect from the client
				}
			} catch (IOException e) {
				System.out.println("Caught exception " + e);
			}
		}
	}

	// -------------------------------------------------------- PRIVATE METHODS

	/**
	 * Read a line from InputStream 's', terminated by CRLF. The CRLF is not
	 * included in the returned string.
	 */
	private static String getLine(InputStream s) throws IOException {
		boolean done = false;
		String result = "";

		while (!done) {
			int ch = s.read(); // Read
			if (ch <= 0 || ch == 10) {
				// Something < 0 means end of data (closed socket)
				// ASCII 10 (line feed) means end of line
				done = true;
			} else if (ch >= ' ') {
				result += (char) ch;
			}
		}

		return result;
	}

	/**
	 * Send a line on OutputStream 's', terminated by CRLF. The CRLF should not
	 * be included in the string str.
	 */
	private static void putLine(OutputStream s, String str) throws IOException {
		s.write(str.getBytes());
		s.write(CRLF);
	}

	private static final byte[] CRLF = { 13, 10 };
}